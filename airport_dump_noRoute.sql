CREATE DATABASE  IF NOT EXISTS `airlinedb`;
USE `airlinedb`;
-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: airlinedb
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `airport`
--

DROP TABLE IF EXISTS `airport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `airport` (
  `airportID` int(11) NOT NULL,
  `leg_key` bigint(20) DEFAULT NULL,
  `leg_name` varchar(15) DEFAULT NULL,
  `leg_city` varchar(14) DEFAULT NULL,
  `leg_type` varchar(13) DEFAULT NULL,
  `leg_radar_type` varchar(3) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`airportID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `airport`
--

LOCK TABLES `airport` WRITE;
/*!40000 ALTER TABLE `airport` DISABLE KEYS */;
INSERT INTO `airport` VALUES (1,3,'Logan','Boston','Single Runway','ILS','MA'),(2,7,'O Hare','Chicago','Multi Runway','ILS','IL'),(3,8,'Midway','Chicago','Multi Runway','VFR','IL'),(4,10,'John Wayne','Costa Mesa','Multi Runway','VFR','CA'),(5,11,'DFW','Dallas','Multi Runway','ILS','TX'),(6,4,'Stapleton','Denver','Multi Runway','VFR','CO'),(7,6,'LAX','Los Angeles','Single Runway','VFR','CA'),(8,12,'Miami','Miami','Single Runway','VFR','FL'),(9,13,'Minneapolis','Minneapolis','Multi Runway','ILS','MN'),(10,20,'Nashville','Nashville','Multi Runway','VFR','TN'),(11,1,'JFK','New York','Multi Runway','ILS','NY'),(12,2,'La Guardia','New York','Multi Runway','VFR','NY'),(13,17,'Philadephia','Philadelphia','Multi Runway','ILS','PA'),(14,14,'Portland','Portland','Multi Runway','VFR','OR'),(15,16,'Raleigh Durham','Raleigh Durham','Multi Runway','VFR','NC'),(16,5,'Lindbergh Field','San Diego','Multi Runway','ILS','CA'),(17,9,'Seattle','Seattle','Single Runway','ILS','WA'),(18,15,'St. Louis','St. Louis','Single Runway','ILS','MO'),(19,18,'Dulles','Washington','Single Runway','VFR','DC'),(20,19,'National','Washington','Multi Runway','ILS','DC');
/*!40000 ALTER TABLE `airport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel`
--

DROP TABLE IF EXISTS `channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `channel` (
  `channelID` int(11) NOT NULL,
  `channel_key` bigint(20) DEFAULT NULL,
  `channel_name` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`channelID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel`
--

LOCK TABLES `channel` WRITE;
/*!40000 ALTER TABLE `channel` DISABLE KEYS */;
INSERT INTO `channel` VALUES (1,1,'Cash'),(2,2,'Credit Card'),(3,3,'Debit Card'),(4,4,'PayPal');
/*!40000 ALTER TABLE `channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `customer` (
  `customerID` int(11) NOT NULL,
  `customer_key` bigint(20) DEFAULT NULL,
  `customer_name` varchar(9) DEFAULT NULL,
  `customer_address` varchar(15) DEFAULT NULL,
  `customer_city` varchar(17) DEFAULT NULL,
  `customer_state` varchar(14) DEFAULT NULL,
  `customer_zip` bigint(20) DEFAULT NULL,
  `customer_type` varchar(13) DEFAULT NULL,
  `customer_income` varchar(8) DEFAULT NULL,
  `customer_birth_date` varchar(19) DEFAULT NULL,
  `customer_marital` varchar(10) DEFAULT NULL,
  `customer_sex` char(1) DEFAULT NULL,
  PRIMARY KEY (`customerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,1,'Anderson','1607 Shady Lane','Birmingham','Alabama',40928,'Employed','102.0','marzo 12, 1956','Married','M'),(2,2,'Antoni','3859 Shady Lane','Tuscaloosa','Alabama',35294,'Employed','35.0','marzo 18, 1936','Married','F'),(3,3,'Appley','1923 Shady Lane','Anchorage','Alaska',58358,'Employed','47.0','febrero 2, 1937','Married','M'),(4,4,'Ashby','9369 Shady Lane','Juneau','Alaska',90421,'Employed','94.0','julio 20, 1959','Married','F'),(5,5,'Barr','7593 Shady Lane','Flagstaff','Arizona',67536,'Employed','93.0','noviembre 30, 1978','Married','M'),(6,6,'Barrett','5332 Shady Lane','Phoenix','Arizona',88392,'Employed','117.0','junio 23, 1982','Married','F'),(7,7,'Bennett','4116 Shady Lane','Little Rock','Arkansas',23848,'Self Employed','35.0','mayo 17, 1976','Married','M'),(8,8,'Boone','3100 Shady Lane','Midville','Arkansas',88536,'Employed','93.0','noviembre 7, 1974','Coresident','F'),(9,9,'Clarke','7808 Shady Lane','San Diego','California',39238,'Employed','61.0','febrero 14, 1949','Single','M'),(10,10,'Clewett','3997 Shady Lane','Red Bluff','California',37374,'Employed','87.0','mayo 31, 1980','Single','F'),(11,11,'Cluster','8640 Shady Lane','Denver','Colorado',61892,'Military','44.0','octubre 18, 1978','Married','M'),(12,12,'Coghlin','7143 Shady Lane','Steamboat Springs','Colorado',92682,'Self Employed','44.0','julio 8, 1968','Married','F'),(13,13,'Davis','8765 Shady Lane','Hartford','Connecticut',94452,'Employed','91.0','junio 22, 1963','Married','M'),(14,14,'DePalma','8778 Shady Lane','Stamford','Connecticut',82401,'Employed','99.0','diciembre 25, 1945','Married','F'),(15,15,'Deardorff','8151 Shady Lane','Wilmington','Delaware',40860,'Employed','62.0','agosto 22, 1957','Married','M'),(16,16,'Dodds','5016 Shady Lane','Ashton','Delaware',95319,'Employed','119.0','julio 27, 1962','Married','F'),(17,17,'Edwards','1789 Shady Lane','Sarasota','Florida',38087,'Employed','33.0','septiembre 28, 1958','Married','M'),(18,18,'Edholm','297 Shady Lane','Miami','Florida',36986,'Employed','30.0','diciembre 11, 1985','Coresident','F'),(19,19,'Emory','7863 Shady Lane','Norcross','Georgia',75225,'Employed','55.0','junio 30, 1973','Single','M'),(20,20,'Erickson','2254 Shady Lane','Augusta','Georgia',60997,'Employed','113.0','noviembre 23, 1932','Single','F'),(21,21,'Farrell','6957 Shady Lane','Kona','Hawaii',92508,'Employed','75.0','octubre 30, 1952','Married','M'),(22,22,'Faulkner','3374 Shady Lane','Hilo','Hawaii',32709,'Employed','70.0','febrero 20, 1974','Married','F'),(23,23,'Foley','311 Shady Lane','Ketchum','Idaho',14818,'Employed','30.0','diciembre 8, 1972','Married','M'),(24,24,'Ford','119 Shady Lane','Sun Valley','Idaho',71699,'Employed','76.0','enero 27, 1987','Married','F'),(25,25,'Gage','4697 Shady Lane','Chicago','Illinois',14621,'Employed','30.0','abril 2, 1942','Married','M'),(26,26,'Galt','1551 Shady Lane','Decatur','Illinois',84668,'Employed','81.0','mayo 11, 1947','Married','F'),(27,27,'Gray','1751 Shady Lane','Lafayette','Indiana',18714,'Military','54.0','mayo 29, 1982','Married','M'),(28,28,'Guy','5582 Shady Lane','Indianapolis','Indiana',43653,'Employed','69.0','abril 26, 1961','Coresident','F'),(29,29,'Haines','7770 Shady Lane','Des Moines','Iowa',15595,'Employed','28.0','enero 19, 1976','Single','M'),(30,30,'Hale','584 Shady Lane','Council Bluffs','Iowa',37536,'Employed','77.0','julio 18, 1954','Single','F'),(31,31,'Hatch','5892 Shady Lane','Topeka','Kansas',17545,'Employed','98.0','marzo 26, 1981','Married','M'),(32,32,'Hatton','3440 Shady Lane','Kansas City','Kansas',98425,'Employed','86.0','septiembre 22, 1978','Married','F'),(33,33,'Ide','4017 Shady Lane','Lexington','Kentucky',47959,'Military','114.0','diciembre 28, 1932','Married','M'),(34,34,'Ilgen','6407 Shady Lane','Blueridge','Kentucky',97712,'Military','24.0','enero 26, 1955','Married','F'),(35,35,'Infante','928 Shady Lane','New Orleans','Louisiana',26814,'Military','73.0','septiembre 24, 1949','Married','M'),(36,36,'Isbell','4209 Shady Lane','Shreveport','Louisiana',55895,'Employed','72.0','enero 28, 1963','Married','F'),(37,37,'Jackson','8614 Shady Lane','Bethesda','Maryland',11802,'Employed','80.0','marzo 20, 1948','Married','M'),(38,38,'Jarrett','9820 Shady Lane','Baltimore','Maryland',20353,'Employed','74.0','junio 5, 1947','Coresident','F'),(39,39,'Jensen','8195 Shady Lane','Augusta','Maine',58098,'Employed','84.0','enero 3, 1954','Single','M'),(40,40,'Johnson','9683 Shady Lane','Bangor','Maine',45251,'Employed','83.0','noviembre 12, 1966','Single','F'),(41,41,'Kessler','3302 Shady Lane','Boston','Massachusetts',42680,'Self Employed','61.0','abril 26, 1978','Married','M'),(42,42,'Kitch','361 Shady Lane','Greenfield','Massachusetts',75026,'Employed','60.0','mayo 7, 1935','Married','F'),(43,43,'Knipe','6874 Shady Lane','Ann Arbor','Michigan',54578,'Employed','113.0','diciembre 5, 1964','Married','M'),(44,44,'Knutsen','6831 Shady Lane','Grand Rapids','Michigan',53680,'Employed','110.0','octubre 2, 1974','Married','F'),(45,45,'Lambert','8693 Shady Lane','Duluth','Minnesota',98704,'Employed','112.0','junio 19, 1965','Married','M'),(46,46,'Landino','700 Shady Lane','Minneapolis','Minnesota',23552,'Military','62.0','febrero 16, 1986','Married','F'),(47,47,'Laplante','4331 Shady Lane','Biloxi','Mississippi',60220,'Employed','44.0','marzo 19, 1933','Married','M'),(48,48,'Larkin','3497 Shady Lane','Jackson','Mississippi',73434,'Employed','89.0','julio 27, 1937','Coresident','F'),(49,49,'McClellan','2387 Shady Lane','St. Louis','Missouri',51907,'Employed','97.0','octubre 15, 1965','Single','M'),(50,50,'Minkel','4548 Shady Lane','Springfield','Missouri',97940,'Military','47.0','febrero 23, 1971','Single','F'),(51,51,'Moran','8147 Shady Lane','Bozeman','Montana',46909,'Employed','109.0','noviembre 15, 1963','Married','M'),(52,52,'Morton','8381 Shady Lane','Butte','Montana',32100,'Employed','49.0','agosto 29, 1983','Married','F'),(53,53,'Nielson','9511 Shady Lane','Lincoln','Nebraska',32541,'Employed','100.0','marzo 1, 1934','Married','M'),(54,54,'Noram','4839 Shady Lane','North Platte','Nebraska',53476,'Employed','74.0','octubre 18, 1968','Married','F'),(55,55,'Norquist','6746 Shady Lane','Tonopah','Nevada',12955,'Employed','106.0','junio 6, 1965','Married','M'),(56,56,'Novick','9448 Shady Lane','Carson City','Nevada',88322,'Self Employed','20.0','octubre 23, 1961','Married','F'),(57,57,'Oakden','6294 Shady Lane','Manchester','New Hampshire',95751,'Employed','91.0','enero 18, 1959','Married','M'),(58,58,'Ortiz','7814 Shady Lane','Concord','New Hampshire',62038,'Employed','116.0','marzo 4, 1987','Coresident','F'),(59,59,'Orson','6999 Shady Lane','Trenton','New Jersey',57639,'Employed','61.0','febrero 1, 1973','Single','M'),(60,60,'Osborne','3438 Shady Lane','Princeton','New Jersey',38666,'Employed','58.0','abril 20, 1977','Single','F'),(61,61,'Page','4480 Shady Lane','Albuquerque','New Mexico',65792,'Employed','47.0','julio 15, 1964','Married','M'),(62,62,'Paskins','1491 Shady Lane','Taos','New Mexico',39260,'Employed','43.0','octubre 4, 1940','Married','F'),(63,63,'Pavlovich','5698 Shady Lane','New York','New York',81372,'Employed','81.0','septiembre 14, 1956','Married','M'),(64,64,'Perez','4751 Shady Lane','Albany','New York',17205,'Military','34.0','marzo 8, 1960','Married','F'),(65,65,'Queen','7562 Shady Lane','Raleigh','North Carolina',84169,'Employed','84.0','noviembre 26, 1975','Married','M'),(66,66,'Quick','4374 Shady Lane','Greensboro','North Carolina',37164,'Employed','89.0','abril 5, 1972','Married','F'),(67,67,'Quinn','3930 Shady Lane','Fargo','North Dakota',23152,'Military','50.0','octubre 17, 1971','Married','M'),(68,68,'Quiroz','5488 Shady Lane','Bismarck','North Dakota',14594,'Employed','70.0','septiembre 25, 1978','Coresident','F'),(69,69,'Rasmussen','9033 Shady Lane','Oklahoma City','Oklahoma',75116,'Employed','27.0','diciembre 30, 1967','Single','M'),(70,70,'Redfern','8296 Shady Lane','Lawton','Oklahoma',95652,'Employed','31.0','noviembre 30, 1975','Single','F'),(71,71,'Richards','6927 Shady Lane','Cincinnati','Ohio',91307,'Self Employed','115.0','marzo 14, 1965','Married','M'),(72,72,'Rivera','2521 Shady Lane','Cleveland','Ohio',46070,'Self Employed','87.0','mayo 18, 1941','Married','F'),(73,73,'Sanson','8599 Shady Lane','Portland','Oregon',95066,'Military','34.0','febrero 24, 1977','Married','M'),(74,74,'Sargent','5161 Shady Lane','Medford','Oregon',56299,'Employed','75.0','octubre 2, 1976','Married','F'),(75,75,'Siegel','4956 Shady Lane','Pittsburgh','Pennsylvania',33260,'Employed','68.0','diciembre 27, 1946','Married','M'),(76,76,'Spicer','3786 Shady Lane','Philadelphia','Pennsylvania',41681,'Employed','20.0','noviembre 16, 1967','Married','F'),(77,77,'Thomas','8946 Shady Lane','Providence','Rhode Island',89540,'Employed','115.0','octubre 18, 1977','Married','M'),(78,78,'Thompspn','6530 Shady Lane','Newport','Rhode Island',77817,'Self Employed','100.0','octubre 20, 1971','Coresident','F'),(79,79,'Turner','2629 Shady Lane','Columbia','South Carolina',39994,'Military','40.0','noviembre 8, 1985','Single','M'),(80,80,'Tuttle','8859 Shady Lane','Charleston','South Carolina',16448,'Self Employed','100.0','febrero 18, 1968','Single','F'),(81,81,'Uphoff','2944 Shady Lane','Rapid City','South Dakota',79528,'Employed','30.0','febrero 13, 1935','Married','M'),(82,82,'Upton','6511 Shady Lane','Pierre','South Dakota',74958,'Employed','32.0','julio 4, 1946','Married','F'),(83,83,'Urbani','1686 Shady Lane','Nashville','Tennessee',91119,'Military','80.0','septiembre 18, 1937','Married','M'),(84,84,'Utzig','8841 Shady Lane','Knoxville','Tennessee',99749,'Employed','94.0','marzo 8, 1972','Married','F'),(85,85,'Valdez','6522 Shady Lane','Dallas','Texas',72383,'Employed','24.0','julio 16, 1977','Married','M'),(86,86,'Vargas','2743 Shady Lane','San Antonio','Texas',92092,'Employed','20.0','marzo 6, 1966','Married','F'),(87,87,'Verch','783 Shady Lane','Salt Lake City','Utah',25394,'Employed','73.0','febrero 20, 1972','Married','M'),(88,88,'Vogel','2436 Shady Lane','Ogden','Utah',85786,'Employed','81.0','enero 16, 1962','Coresident','F'),(89,89,'Wadsworth','7902 Shady Lane','Burlington','Vermont',29451,'Employed','41.0','mayo 21, 1982','Single','M'),(90,90,'Walsh','6442 Shady Lane','Montpelier','Vermont',18957,'Employed','97.0','septiembre 24, 1959','Single','F'),(91,91,'Watson','8664 Shady Lane','Richmond','Virginia',79762,'Self Employed','102.0','diciembre 2, 1935','Married','M'),(92,92,'Wedig','1269 Shady Lane','Norfolk','Virginia',95728,'Self Employed','21.0','mayo 23, 1951','Married','F'),(93,93,'Xanadu','8834 Shady Lane','Seattle','Washington',74273,'Employed','92.0','septiembre 29, 1947','Married','M'),(94,94,'Xavier','7326 Shady Lane','Tacoma','Washington',77589,'Employed','86.0','diciembre 31, 1943','Married','F'),(95,95,'Xiong','3835 Shady Lane','Charleston','West Virginia',56334,'Employed','39.0','diciembre 7, 1970','Married','M'),(96,96,'Xiques','6779 Shady Lane','Clarksburg','West Virginia',37184,'Employed','50.0','septiembre 14, 1955','Married','F'),(97,97,'Yost','8214 Shady Lane','Madison','Wisconsin',42262,'Military','57.0','enero 7, 1985','Married','M'),(98,98,'Yoder','4103 Shady Lane','Milwaukee','Wisconsin',95392,'Employed','33.0','junio 25, 1979','Coresident','F'),(99,99,'Young','6328 Shady Lane','Laramie','Wyoming',81373,'Employed','44.0','diciembre 18, 1953','Single','M'),(100,100,'Youpa','6905 Shady Lane','Cheyenne','Wyoming',98309,'Self Employed','75.0','diciembre 20, 1940','Single','F');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `date`
--

DROP TABLE IF EXISTS `date`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `date` (
  `dateID` int(11) NOT NULL,
  `day` int(2) DEFAULT NULL,
  `month` int(2) DEFAULT NULL,
  `year` int(4) DEFAULT NULL,
  PRIMARY KEY (`dateID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `date`
--

LOCK TABLES `date` WRITE;
/*!40000 ALTER TABLE `date` DISABLE KEYS */;
INSERT INTO `date` VALUES (1,1,1,2018),(2,2,1,2018),(3,3,1,2018),(4,4,1,2018),(5,5,1,2018),(6,6,1,2018),(7,7,1,2018),(8,8,1,2018),(9,9,1,2018),(10,10,1,2018),(11,11,1,2018),(12,12,1,2018),(13,13,1,2018),(14,14,1,2018),(15,15,1,2018),(16,16,1,2018),(17,17,1,2018),(18,18,1,2018),(19,19,1,2018),(20,20,1,2018),(21,21,1,2018),(22,22,1,2018),(23,23,1,2018),(24,24,1,2018),(25,25,1,2018),(26,26,1,2018),(27,27,1,2018),(28,28,1,2018),(29,29,1,2018),(30,30,1,2018),(31,31,1,2018),(32,1,2,2018),(33,2,2,2018),(34,3,2,2018),(35,4,2,2018),(36,5,2,2018),(37,6,2,2018),(38,7,2,2018),(39,8,2,2018),(40,9,2,2018),(41,10,2,2018),(42,11,2,2018),(43,12,2,2018),(44,13,2,2018),(45,14,2,2018),(46,15,2,2018),(47,16,2,2018),(48,17,2,2018),(49,18,2,2018),(50,19,2,2018),(51,20,2,2018),(52,21,2,2018),(53,22,2,2018),(54,23,2,2018),(55,24,2,2018),(56,25,2,2018),(57,26,2,2018),(58,27,2,2018),(59,28,2,2018),(60,1,3,2018),(61,2,3,2018),(62,3,3,2018),(63,4,3,2018),(64,5,3,2018),(65,6,3,2018),(66,7,3,2018),(67,8,3,2018),(68,9,3,2018),(69,10,3,2018),(70,11,3,2018),(71,12,3,2018),(72,13,3,2018),(73,14,3,2018),(74,15,3,2018),(75,16,3,2018),(76,17,3,2018),(77,18,3,2018),(78,19,3,2018),(79,20,3,2018),(80,21,3,2018),(81,22,3,2018),(82,23,3,2018),(83,24,3,2018),(84,25,3,2018),(85,26,3,2018),(86,27,3,2018),(87,28,3,2018),(88,29,3,2018),(89,30,3,2018),(90,31,3,2018),(91,1,4,2018),(92,2,4,2018),(93,3,4,2018),(94,4,4,2018),(95,5,4,2018),(96,6,4,2018),(97,7,4,2018),(98,8,4,2018),(99,9,4,2018),(100,10,4,2018),(101,11,4,2018),(102,12,4,2018),(103,13,4,2018),(104,14,4,2018),(105,15,4,2018),(106,16,4,2018),(107,17,4,2018),(108,18,4,2018),(109,19,4,2018),(110,20,4,2018),(111,21,4,2018),(112,22,4,2018),(113,23,4,2018),(114,24,4,2018),(115,25,4,2018),(116,26,4,2018),(117,27,4,2018),(118,28,4,2018),(119,29,4,2018),(120,30,4,2018),(121,1,5,2018),(122,2,5,2018),(123,3,5,2018),(124,4,5,2018),(125,5,5,2018),(126,6,5,2018),(127,7,5,2018),(128,8,5,2018),(129,9,5,2018),(130,10,5,2018),(131,11,5,2018),(132,12,5,2018),(133,13,5,2018),(134,14,5,2018),(135,15,5,2018),(136,16,5,2018),(137,17,5,2018),(138,18,5,2018),(139,19,5,2018),(140,20,5,2018),(141,21,5,2018),(142,22,5,2018),(143,23,5,2018),(144,24,5,2018),(145,25,5,2018),(146,26,5,2018),(147,27,5,2018),(148,28,5,2018),(149,29,5,2018),(150,30,5,2018),(151,31,5,2018),(152,1,6,2018),(153,2,6,2018),(154,3,6,2018),(155,4,6,2018),(156,5,6,2018),(157,6,6,2018),(158,7,6,2018),(159,8,6,2018),(160,9,6,2018),(161,10,6,2018),(162,11,6,2018),(163,12,6,2018),(164,13,6,2018),(165,14,6,2018),(166,15,6,2018),(167,16,6,2018),(168,17,6,2018),(169,18,6,2018),(170,19,6,2018),(171,20,6,2018),(172,21,6,2018),(173,22,6,2018),(174,23,6,2018),(175,24,6,2018),(176,25,6,2018),(177,26,6,2018),(178,27,6,2018),(179,28,6,2018),(180,29,6,2018),(181,30,6,2018),(182,1,7,2018),(183,2,7,2018),(184,3,7,2018),(185,4,7,2018),(186,5,7,2018),(187,6,7,2018),(188,7,7,2018),(189,8,7,2018),(190,9,7,2018),(191,10,7,2018),(192,11,7,2018),(193,12,7,2018),(194,13,7,2018),(195,14,7,2018),(196,15,7,2018),(197,16,7,2018),(198,17,7,2018),(199,18,7,2018),(200,19,7,2018),(201,20,7,2018),(202,21,7,2018),(203,22,7,2018),(204,23,7,2018),(205,24,7,2018),(206,25,7,2018),(207,26,7,2018),(208,27,7,2018),(209,28,7,2018),(210,29,7,2018),(211,30,7,2018),(212,31,7,2018),(213,1,8,2018),(214,2,8,2018),(215,3,8,2018),(216,4,8,2018),(217,5,8,2018),(218,6,8,2018),(219,7,8,2018),(220,8,8,2018),(221,9,8,2018),(222,10,8,2018),(223,11,8,2018),(224,12,8,2018),(225,13,8,2018),(226,14,8,2018),(227,15,8,2018),(228,16,8,2018),(229,17,8,2018),(230,18,8,2018),(231,19,8,2018),(232,20,8,2018),(233,21,8,2018),(234,22,8,2018),(235,23,8,2018),(236,24,8,2018),(237,25,8,2018),(238,26,8,2018),(239,27,8,2018),(240,28,8,2018),(241,29,8,2018),(242,30,8,2018),(243,31,8,2018),(244,1,9,2018),(245,2,9,2018),(246,3,9,2018),(247,4,9,2018),(248,5,9,2018),(249,6,9,2018),(250,7,9,2018),(251,8,9,2018),(252,9,9,2018),(253,10,9,2018),(254,11,9,2018),(255,12,9,2018),(256,13,9,2018),(257,14,9,2018),(258,15,9,2018),(259,16,9,2018),(260,17,9,2018),(261,18,9,2018),(262,19,9,2018),(263,20,9,2018),(264,21,9,2018),(265,22,9,2018),(266,23,9,2018),(267,24,9,2018),(268,25,9,2018),(269,26,9,2018),(270,27,9,2018),(271,28,9,2018),(272,29,9,2018),(273,30,9,2018),(274,1,10,2018),(275,2,10,2018),(276,3,10,2018),(277,4,10,2018),(278,5,10,2018),(279,6,10,2018),(280,7,10,2018),(281,8,10,2018),(282,9,10,2018),(283,10,10,2018),(284,11,10,2018),(285,12,10,2018),(286,13,10,2018),(287,14,10,2018),(288,15,10,2018),(289,16,10,2018),(290,17,10,2018),(291,18,10,2018),(292,19,10,2018),(293,20,10,2018),(294,21,10,2018),(295,22,10,2018),(296,23,10,2018),(297,24,10,2018),(298,25,10,2018),(299,26,10,2018),(300,27,10,2018),(301,28,10,2018),(302,29,10,2018),(303,30,10,2018),(304,31,10,2018),(305,1,11,2018),(306,2,11,2018),(307,3,11,2018),(308,4,11,2018),(309,5,11,2018),(310,6,11,2018),(311,7,11,2018),(312,8,11,2018),(313,9,11,2018),(314,10,11,2018),(315,11,11,2018),(316,12,11,2018),(317,13,11,2018),(318,14,11,2018),(319,15,11,2018),(320,16,11,2018),(321,17,11,2018),(322,18,11,2018),(323,19,11,2018),(324,20,11,2018),(325,21,11,2018),(326,22,11,2018),(327,23,11,2018),(328,24,11,2018),(329,25,11,2018),(330,26,11,2018),(331,27,11,2018),(332,28,11,2018),(333,29,11,2018),(334,30,11,2018),(335,1,12,2018),(336,2,12,2018),(337,3,12,2018),(338,4,12,2018),(339,5,12,2018),(340,6,12,2018),(341,7,12,2018),(342,8,12,2018),(343,9,12,2018),(344,10,12,2018),(345,11,12,2018),(346,12,12,2018),(347,13,12,2018),(348,14,12,2018),(349,15,12,2018),(350,16,12,2018),(351,17,12,2018),(352,18,12,2018),(353,19,12,2018),(354,20,12,2018),(355,21,12,2018),(356,22,12,2018),(357,23,12,2018),(358,24,12,2018),(359,25,12,2018),(360,26,12,2018),(361,27,12,2018),(362,28,12,2018),(363,29,12,2018),(364,30,12,2018),(365,31,12,2018),(366,1,1,2019),(367,2,1,2019),(368,3,1,2019),(369,4,1,2019),(370,5,1,2019),(371,6,1,2019),(372,7,1,2019),(373,8,1,2019),(374,9,1,2019),(375,10,1,2019),(376,11,1,2019),(377,12,1,2019),(378,13,1,2019),(379,14,1,2019),(380,15,1,2019),(381,16,1,2019),(382,17,1,2019),(383,18,1,2019),(384,19,1,2019),(385,20,1,2019),(386,21,1,2019),(387,22,1,2019),(388,23,1,2019),(389,24,1,2019),(390,25,1,2019),(391,26,1,2019),(392,27,1,2019),(393,28,1,2019),(394,29,1,2019),(395,30,1,2019),(396,31,1,2019),(397,1,2,2019),(398,2,2,2019),(399,3,2,2019),(400,4,2,2019),(401,5,2,2019),(402,6,2,2019),(403,7,2,2019),(404,8,2,2019),(405,9,2,2019),(406,10,2,2019),(407,11,2,2019),(408,12,2,2019),(409,13,2,2019),(410,14,2,2019),(411,15,2,2019),(412,16,2,2019),(413,17,2,2019),(414,18,2,2019),(415,19,2,2019),(416,20,2,2019),(417,21,2,2019),(418,22,2,2019),(419,23,2,2019),(420,24,2,2019),(421,25,2,2019),(422,26,2,2019),(423,27,2,2019),(424,28,2,2019),(425,1,3,2019),(426,2,3,2019),(427,3,3,2019),(428,4,3,2019),(429,5,3,2019),(430,6,3,2019),(431,7,3,2019),(432,8,3,2019),(433,9,3,2019),(434,10,3,2019),(435,11,3,2019),(436,12,3,2019),(437,13,3,2019),(438,14,3,2019),(439,15,3,2019),(440,16,3,2019),(441,17,3,2019),(442,18,3,2019),(443,19,3,2019),(444,20,3,2019),(445,21,3,2019),(446,22,3,2019),(447,23,3,2019),(448,24,3,2019),(449,25,3,2019),(450,26,3,2019),(451,27,3,2019),(452,28,3,2019),(453,29,3,2019),(454,30,3,2019),(455,31,3,2019),(456,1,4,2019),(457,2,4,2019),(458,3,4,2019),(459,4,4,2019),(460,5,4,2019),(461,6,4,2019),(462,7,4,2019),(463,8,4,2019),(464,9,4,2019),(465,10,4,2019),(466,11,4,2019),(467,12,4,2019),(468,13,4,2019),(469,14,4,2019),(470,15,4,2019),(471,16,4,2019),(472,17,4,2019),(473,18,4,2019),(474,19,4,2019),(475,20,4,2019),(476,21,4,2019),(477,22,4,2019),(478,23,4,2019),(479,24,4,2019),(480,25,4,2019),(481,26,4,2019),(482,27,4,2019),(483,28,4,2019),(484,29,4,2019),(485,30,4,2019),(486,1,5,2019),(487,2,5,2019),(488,3,5,2019),(489,4,5,2019),(490,5,5,2019),(491,6,5,2019),(492,7,5,2019),(493,8,5,2019),(494,9,5,2019),(495,10,5,2019),(496,11,5,2019),(497,12,5,2019),(498,13,5,2019),(499,14,5,2019),(500,15,5,2019),(501,16,5,2019),(502,17,5,2019),(503,18,5,2019),(504,19,5,2019),(505,20,5,2019),(506,21,5,2019),(507,22,5,2019),(508,23,5,2019),(509,24,5,2019),(510,25,5,2019),(511,26,5,2019),(512,27,5,2019),(513,28,5,2019),(514,29,5,2019),(515,30,5,2019),(516,31,5,2019),(517,1,6,2019),(518,2,6,2019),(519,3,6,2019),(520,4,6,2019),(521,5,6,2019),(522,6,6,2019),(523,7,6,2019),(524,8,6,2019),(525,9,6,2019),(526,10,6,2019),(527,11,6,2019),(528,12,6,2019),(529,13,6,2019),(530,14,6,2019),(531,15,6,2019),(532,16,6,2019),(533,17,6,2019),(534,18,6,2019),(535,19,6,2019),(536,20,6,2019),(537,21,6,2019),(538,22,6,2019),(539,23,6,2019),(540,24,6,2019),(541,25,6,2019),(542,26,6,2019),(543,27,6,2019),(544,28,6,2019),(545,29,6,2019),(546,30,6,2019),(547,1,7,2019),(548,2,7,2019),(549,3,7,2019),(550,4,7,2019),(551,5,7,2019),(552,6,7,2019),(553,7,7,2019),(554,8,7,2019),(555,9,7,2019),(556,10,7,2019),(557,11,7,2019),(558,12,7,2019),(559,13,7,2019),(560,14,7,2019),(561,15,7,2019),(562,16,7,2019),(563,17,7,2019),(564,18,7,2019),(565,19,7,2019),(566,20,7,2019),(567,21,7,2019),(568,22,7,2019),(569,23,7,2019),(570,24,7,2019),(571,25,7,2019),(572,26,7,2019),(573,27,7,2019),(574,28,7,2019),(575,29,7,2019),(576,30,7,2019),(577,31,7,2019),(578,1,8,2019),(579,2,8,2019),(580,3,8,2019),(581,4,8,2019),(582,5,8,2019),(583,6,8,2019),(584,7,8,2019),(585,8,8,2019),(586,9,8,2019),(587,10,8,2019),(588,11,8,2019),(589,12,8,2019),(590,13,8,2019),(591,14,8,2019),(592,15,8,2019),(593,16,8,2019),(594,17,8,2019),(595,18,8,2019),(596,19,8,2019),(597,20,8,2019),(598,21,8,2019),(599,22,8,2019),(600,23,8,2019),(601,24,8,2019),(602,25,8,2019),(603,26,8,2019),(604,27,8,2019),(605,28,8,2019),(606,29,8,2019),(607,30,8,2019),(608,31,8,2019),(609,1,9,2019),(610,2,9,2019),(611,3,9,2019),(612,4,9,2019),(613,5,9,2019),(614,6,9,2019),(615,7,9,2019),(616,8,9,2019),(617,9,9,2019),(618,10,9,2019),(619,11,9,2019),(620,12,9,2019),(621,13,9,2019),(622,14,9,2019),(623,15,9,2019),(624,16,9,2019),(625,17,9,2019),(626,18,9,2019),(627,19,9,2019),(628,20,9,2019),(629,21,9,2019),(630,22,9,2019),(631,23,9,2019),(632,24,9,2019),(633,25,9,2019),(634,26,9,2019),(635,27,9,2019),(636,28,9,2019),(637,29,9,2019),(638,30,9,2019),(639,1,10,2019),(640,2,10,2019),(641,3,10,2019),(642,4,10,2019),(643,5,10,2019),(644,6,10,2019),(645,7,10,2019),(646,8,10,2019),(647,9,10,2019),(648,10,10,2019),(649,11,10,2019),(650,12,10,2019),(651,13,10,2019),(652,14,10,2019),(653,15,10,2019),(654,16,10,2019),(655,17,10,2019),(656,18,10,2019),(657,19,10,2019),(658,20,10,2019),(659,21,10,2019),(660,22,10,2019),(661,23,10,2019),(662,24,10,2019),(663,25,10,2019),(664,26,10,2019),(665,27,10,2019),(666,28,10,2019),(667,29,10,2019),(668,30,10,2019),(669,31,10,2019),(670,1,11,2019),(671,2,11,2019),(672,3,11,2019),(673,4,11,2019),(674,5,11,2019),(675,6,11,2019),(676,7,11,2019),(677,8,11,2019),(678,9,11,2019),(679,10,11,2019),(680,11,11,2019),(681,12,11,2019),(682,13,11,2019),(683,14,11,2019),(684,15,11,2019),(685,16,11,2019),(686,17,11,2019),(687,18,11,2019),(688,19,11,2019),(689,20,11,2019),(690,21,11,2019),(691,22,11,2019),(692,23,11,2019),(693,24,11,2019),(694,25,11,2019),(695,26,11,2019),(696,27,11,2019),(697,28,11,2019),(698,29,11,2019),(699,30,11,2019),(700,1,12,2019),(701,2,12,2019),(702,3,12,2019),(703,4,12,2019),(704,5,12,2019),(705,6,12,2019),(706,7,12,2019),(707,8,12,2019),(708,9,12,2019),(709,10,12,2019),(710,11,12,2019),(711,12,12,2019),(712,13,12,2019),(713,14,12,2019),(714,15,12,2019),(715,16,12,2019),(716,17,12,2019),(717,18,12,2019),(718,19,12,2019),(719,20,12,2019),(720,21,12,2019),(721,22,12,2019),(722,23,12,2019),(723,24,12,2019),(724,25,12,2019),(725,26,12,2019),(726,27,12,2019),(727,28,12,2019),(728,29,12,2019),(729,30,12,2019),(730,31,12,2019);
/*!40000 ALTER TABLE `date` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fare`
--

DROP TABLE IF EXISTS `fare`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `fare` (
  `fareID` int(11) NOT NULL,
  `fare_class_key` bigint(20) DEFAULT NULL,
  `fare_class_code` char(1) DEFAULT NULL,
  `fare_class_description` varchar(8) DEFAULT NULL,
  `restriction_type` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`fareID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fare`
--

LOCK TABLES `fare` WRITE;
/*!40000 ALTER TABLE `fare` DISABLE KEYS */;
INSERT INTO `fare` VALUES (1,1,'Y','Economy','None'),(2,2,'J','Business','None'),(3,3,'F','First','None'),(4,4,'X','Discount','30 Day Advance');
/*!40000 ALTER TABLE `fare` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flight`
--

DROP TABLE IF EXISTS `flight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `flight` (
  `flightID` int(11) NOT NULL,
  `flight_key` bigint(20) DEFAULT NULL,
  `sched_depart` varchar(5) DEFAULT NULL,
  `sched_arrival` varchar(5) DEFAULT NULL,
  `airplane_type` varchar(8) DEFAULT NULL,
  `seat_capacity` bigint(20) DEFAULT NULL,
  `first_class_capacity` bigint(20) DEFAULT NULL,
  `business_capacity` bigint(20) DEFAULT NULL,
  `coach_capacity` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`flightID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flight`
--

LOCK TABLES `flight` WRITE;
/*!40000 ALTER TABLE `flight` DISABLE KEYS */;
INSERT INTO `flight` VALUES (1,1,'8:05','9:29','Super 80',150,14,0,136),(2,2,'8:10','8:47','DC-10',300,28,28,244),(3,3,'8:15','10:13','DC-10',300,28,28,244),(4,4,'8:20','10:37','727',130,16,0,114),(5,5,'8:25','10:21','DC-10',300,28,28,244),(6,6,'8:30','10:22','727',130,16,0,114),(7,7,'8:35','10:59','DC-10',300,28,28,244),(8,8,'8:40','9:36','Super 80',150,14,0,136),(9,9,'8:45','11:09','Super 80',150,14,0,136),(10,10,'8:50','11:47','727',130,16,0,114),(11,11,'8:55','10:35','727',130,16,0,114),(12,12,'9:00','11:46','727',130,16,0,114),(13,13,'9:05','11:13','Super 80',150,14,0,136),(14,14,'9:10','11:19','DC-10',300,28,28,244),(15,15,'9:15','11:03','DC-10',300,28,28,244),(16,16,'9:20','11:59','727',130,16,0,114),(17,17,'9:25','10:25','727',130,16,0,114),(18,18,'9:30','11:42','Super 80',150,14,0,136),(19,19,'9:35','11:45','Super 80',150,14,0,136),(20,20,'9:40','11:41','Super 80',150,14,0,136),(21,21,'9:45','12:20','DC-10',300,28,28,244),(22,22,'9:50','12:46','DC-10',300,28,28,244),(23,23,'9:55','12:31','727',130,16,0,114),(24,24,'10:00','11:04','DC-10',300,28,28,244),(25,25,'10:05','12:27','Super 80',150,14,0,136),(26,26,'10:10','11:31','727',130,16,0,114),(27,27,'10:15','12:28','727',130,16,0,114),(28,28,'10:20','12:24','DC-10',300,28,28,244),(29,29,'10:25','11:17','727',130,16,0,114),(30,30,'10:30','11:55','DC-10',300,28,28,244),(31,31,'10:35','12:05','Super 80',150,14,0,136),(32,32,'10:40','12:21','727',130,16,0,114),(33,33,'10:45','12:17','727',130,16,0,114),(34,34,'10:50','12:07','DC-10',300,28,28,244),(35,35,'10:55','13:30','DC-10',300,28,28,244),(36,36,'11:00','13:51','727',130,16,0,114),(37,37,'11:05','12:21','727',130,16,0,114),(38,38,'11:10','14:00','DC-10',300,28,28,244),(39,39,'11:15','14:03','DC-10',300,28,28,244),(40,40,'11:20','12:10','DC-10',300,28,28,244),(41,41,'11:25','14:02','727',130,16,0,114),(42,42,'11:30','13:12','DC-10',300,28,28,244),(43,43,'11:35','13:45','727',130,16,0,114),(44,44,'11:40','12:35','Super 80',150,14,0,136),(45,45,'11:45','12:38','Super 80',150,14,0,136),(46,46,'11:50','14:39','DC-10',300,28,28,244),(47,47,'11:55','13:02','DC-10',300,28,28,244),(48,48,'12:00','13:04','DC-10',300,28,28,244),(49,49,'12:05','14:54','Super 80',150,14,0,136),(50,50,'12:10','12:58','DC-10',300,28,28,244),(51,51,'12:15','13:20','727',130,16,0,114),(52,52,'12:20','14:03','Super 80',150,14,0,136),(53,53,'12:25','14:06','DC-10',300,28,28,244),(54,54,'12:30','15:29','Super 80',150,14,0,136),(55,55,'12:35','14:17','DC-10',300,28,28,244),(56,56,'12:40','13:34','727',130,16,0,114),(57,57,'12:45','14:31','Super 80',150,14,0,136),(58,58,'12:50','14:08','DC-10',300,28,28,244),(59,59,'12:55','14:50','DC-10',300,28,28,244),(60,60,'13:00','14:53','727',130,16,0,114),(61,61,'13:05','14:07','Super 80',150,14,0,136),(62,62,'13:10','14:59','727',130,16,0,114),(63,63,'13:15','14:59','727',130,16,0,114),(64,64,'13:20','15:11','727',130,16,0,114),(65,65,'13:25','15:07','Super 80',150,14,0,136),(66,66,'13:30','15:35','727',130,16,0,114),(67,67,'13:35','14:07','Super 80',150,14,0,136),(68,68,'13:40','16:11','727',130,16,0,114),(69,69,'13:45','14:22','727',130,16,0,114),(70,70,'13:50','16:48','Super 80',150,14,0,136),(71,71,'13:55','15:47','727',130,16,0,114),(72,72,'14:00','16:22','DC-10',300,28,28,244),(73,73,'14:05','16:53','DC-10',300,28,28,244),(74,74,'14:10','15:58','727',130,16,0,114),(75,75,'14:15','14:52','727',130,16,0,114),(76,76,'14:20','16:42','Super 80',150,14,0,136),(77,77,'14:25','16:08','Super 80',150,14,0,136),(78,78,'14:30','16:24','DC-10',300,28,28,244),(79,79,'14:35','15:34','Super 80',150,14,0,136),(80,80,'14:40','15:13','DC-10',300,28,28,244),(81,81,'14:45','15:56','727',130,16,0,114),(82,82,'14:50','16:36','DC-10',300,28,28,244),(83,83,'14:55','16:00','Super 80',150,14,0,136),(84,84,'15:00','15:40','DC-10',300,28,28,244),(85,85,'15:05','15:41','727',130,16,0,114),(86,86,'15:10','17:56','727',130,16,0,114),(87,87,'15:15','16:38','Super 80',150,14,0,136),(88,88,'15:20','18:18','DC-10',300,28,28,244),(89,89,'15:25','16:27','DC-10',300,28,28,244),(90,90,'15:30','16:39','Super 80',150,14,0,136),(91,91,'15:35','18:25','DC-10',300,28,28,244),(92,92,'15:40','16:10','Super 80',150,14,0,136),(93,93,'15:45','16:22','727',130,16,0,114),(94,94,'15:50','18:22','Super 80',150,14,0,136),(95,95,'15:55','17:05','Super 80',150,14,0,136),(96,96,'16:00','18:44','727',130,16,0,114),(97,97,'16:05','18:59','Super 80',150,14,0,136),(98,98,'16:10','19:00','727',130,16,0,114),(99,99,'16:15','19:07','DC-10',300,28,28,244),(100,100,'16:20','18:06','Super 80',150,14,0,136);
/*!40000 ALTER TABLE `flight` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `route`
--

DROP TABLE IF EXISTS `route`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `route` (
  `flown_key` bigint(20) NOT NULL,
  `fare_price` bigint(20) DEFAULT NULL,
  `miles` bigint(20) DEFAULT NULL,
  `minutes_late` bigint(20) DEFAULT NULL,
  `ticket_number` bigint(20) NOT NULL,
  `aeropuerto_origen_trayecto` int(11) NOT NULL,
  `aeropuerto_origen_itinerario` int(11) NOT NULL,
  `aeropuerto_destino_trayecto` int(11) NOT NULL,
  `aeropuerto_destino_itinerario` int(11) NOT NULL,
  `customer` int(11) NOT NULL,
  `channel` int(11) NOT NULL,
  `fare_key` int(11) NOT NULL,
  `flight` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  PRIMARY KEY (`flown_key`,`ticket_number`,`aeropuerto_origen_trayecto`,`aeropuerto_origen_itinerario`,`aeropuerto_destino_trayecto`,`aeropuerto_destino_itinerario`,`customer`,`channel`,`date`,`fare_key`,`flight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `route`
--

LOCK TABLES `route` WRITE;
/*!40000 ALTER TABLE `route` DISABLE KEYS */;
/*!40000 ALTER TABLE `route` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-02 16:32:58
